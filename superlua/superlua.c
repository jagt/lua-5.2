#include <stdio.h>
#include <stdlib.h>
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#define err(msg) do {fprintf(stderr, "error: %s\n", (msg)); exit(1);} while (0)

int super_bind(lua_State *L) {
    return 0;
}

static const luaL_Reg superlib[] = {
    {"bind", super_bind},
    {NULL, NULL}
};

int luaopen_super(lua_State *L) {
    luaL_newlib(L, superlib);
    return 1;
}

int main(int argc, char* argv[]) {
    lua_State *L = luaL_newstate();

    if (argc < 2) {
        puts("usage: superlua foo.lua");
        exit(0);
    }
    if (L == NULL)
        err("failed to create lua state");
    luaL_checkversion(L);
    lua_gc(L, LUA_GCSTOP, 0);  /* stop collector during initialization */
    luaL_openlibs(L);  /* open libraries */

    // open super lib
    luaL_requiref(L, "super", luaopen_super, 1);
    lua_pop(L, 1);

    lua_gc(L, LUA_GCRESTART, 0);

    if (luaL_dofile(L, argv[1])) {
        // report the error
        err(lua_tolstring(L, -1, 0));
    }

    return 0;
}
