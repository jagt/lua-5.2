solution('superlua')
project('superlua')
configurations{'Debug', 'Release'}
language('C')
flags {'ExtraWarnings'}
kind('ConsoleApp')
libdirs('../install/lib')
includedirs('../install/include')
files('*.c')
links{'m', 'dl'}
linkoptions {'-llua'}

premake.gcc.cc='clang'
premake.gcc.cxx='clang++'
